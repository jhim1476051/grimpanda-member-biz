package com.factory.demo.member.dto;

import com.factory.demo.member.model.MemberType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LoginResultDto{

    private Boolean login;
    private String email;
    private String hpNo;
    private String name;
    private String nickname;
    private MemberType memberType;
    private Boolean status;
}
