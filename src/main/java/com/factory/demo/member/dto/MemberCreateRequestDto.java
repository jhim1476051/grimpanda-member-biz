package com.factory.demo.member.dto;

import com.factory.demo.member.model.Member;
import com.factory.demo.member.model.MemberType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@ToString
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
@Schema(title = "Member Creation request")
public class MemberCreateRequestDto {

    private String email;
    private String hpNo;
    private String name;
    private String password;
    private String nickname;
    @Builder.Default
    private MemberType memberType = MemberType.CUSTOMER;
    @Builder.Default
    private Boolean status = true;

//    @Builder
//    public MemberCreateRequestDto(String email, String hpNo, String name, String password, String nickname
//            , MemberType memberType, Boolean status
//    ) {
//        this.email = email;
//        this.hpNo = hpNo;
//        this.name = name;
//        this.password = password;
//        this.nickname = nickname;
//        this.memberType = memberType;
//        this.status = status;
//    }

    // old
//    @Builder
//    public MemberCreateRequestDto(String name, String nickname, String email, String password) {
//        this.name = name;
//        this.nickname = nickname;
//        this.email = email;
//        this.password = password;
//    }

    public Member toEntity() {
        return Member.builder()
                .email(email)
                .hpNo(hpNo)
                .name(name)
                .password(password)
                .nickname(nickname)
                .memberType(memberType)
                .status(status)
                .build();
    }

}
