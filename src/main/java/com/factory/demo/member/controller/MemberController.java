package com.factory.demo.member.controller;

import com.factory.demo.member.dto.LoginResultDto;
import com.factory.demo.member.dto.MemberCreateRequestDto;
import com.factory.demo.member.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/members")
public class MemberController {

    private final MemberService memberService;

    @PostMapping
    public Long save(@RequestBody MemberCreateRequestDto requestDto) {
        return this.memberService.save(requestDto);
    }

    @GetMapping(
            value = "/{tenantId}")
    public LoginResultDto getMember(@PathVariable(value = "tenantId") String tenantId) {
        return this.memberService.getMemberInfo(tenantId);
    }

}
