package com.factory.demo.member.service.impl;

import com.factory.demo.member.dto.MemberCreateRequestDto;
import com.factory.demo.member.dto.MemberLoginRequestDto;
import com.factory.demo.member.dto.LoginResultDto;
import com.factory.demo.member.dto.LoginSuccessResultDto;
import com.factory.demo.member.model.Member;
import com.factory.demo.member.repository.MemberRepository;
import com.factory.demo.member.service.MemberService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberServiceImpl implements MemberService {

    private final MemberRepository memberRepository;

    @Override
    @Transactional
    public Long save(MemberCreateRequestDto requestDto) {
        // 이미 등록된 Email이면 -1을 return하는 것이 API 스펙이라고 가정함
        Member savedMember = memberRepository.findByEmail(requestDto.getEmail()); // 혹은 Exists 함수를 사용할 수 있음
        if (savedMember != null) {
            return -1L;
        }
        System.out.println("test");
        return this.memberRepository.save(requestDto.toEntity()).getId();
    }

    @Override
    public LoginResultDto login(MemberLoginRequestDto requestDto) {
        Member savedMember = this.memberRepository.findByEmail(requestDto.getEmail());
        if (savedMember == null) {
            return LoginResultDto.builder()
                    .login(false)
                    .build();
        }
        if (!savedMember.getPassword().equals(requestDto.getPassword())) {
            return LoginResultDto.builder()
                    .login(false)
                    .build();
        }
        return LoginResultDto.builder()
                .login(true)
                .email(savedMember.getEmail())
                .hpNo(savedMember.getHpNo())
                .name(savedMember.getName())
                .nickname(savedMember.getNickname())
                .memberType(savedMember.getMemberType())
                .status(savedMember.getStatus())
                .build();
    }

    @Override
    public LoginResultDto getMemberInfo(String nickname) {
        Member m = memberRepository.findByNickname(nickname);
        return LoginResultDto.builder()
                .email(m.getEmail())
                .hpNo(m.getHpNo())
                .name(m.getName())
                .nickname(m.getNickname())
                .memberType(m.getMemberType())
                .status(m.getStatus())
                .build();
//        return null;
    }
}
