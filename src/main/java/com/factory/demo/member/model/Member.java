package com.factory.demo.member.model;

import com.factory.demo.cmn.model.AmfBaseEntity;
import jakarta.persistence.*;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
public class Member extends AmfBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String email;
    private String hpNo;
    private String name;
    private String password;
    private String nickname;
    @Enumerated(EnumType.STRING)
    private MemberType memberType;
    private Boolean status;



}
