package com.factory.demo.member.model;

public enum MemberType {
    CUSTOMER, SUPPLIER, ADMIN
}
